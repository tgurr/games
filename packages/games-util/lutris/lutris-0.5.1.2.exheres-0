# Copyright 2017-2018 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'lutris-0.4.12.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

require github [ user=${PN} tag=v${PV} ] \
    setup-py [ import=setuptools blacklist="2" multibuild=false ] \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="Lutris is an open source gaming platform for Linux"
HOMEPAGE+=" https://lutris.net"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# Require X
RESTRICT="test"

# TODO: bundles app-emulation/winetricks (share/lutris/bin)
DEPENDENCIES="
    build+run:
        dev-python/dbus-python[python_abis:*(-)?]
        dev-python/evdev[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/wheel[python_abis:*(-)?]
        dev-python/Pillow[python_abis:*(-)?]
        dev-python/PyYAML[python_abis:*(-)?]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
        gnome-desktop/gnome-desktop:3.0[gobject-introspection]
        gnome-desktop/libsoup:2.4
        net-libs/webkit:4.0[gobject-introspection]
        sys-apps/pciutils
        x11-apps/xgamma
        x11-apps/xrandr
        x11-libs/gdk-pixbuf:2.0[gobject-introspection]
        x11-libs/gtk+:3[gobject-introspection]
        x11-libs/libnotify[gobject-introspection]
    recommendation:
        app-arch/cabextract [[
            description = [ Required to install some Windows applications via Wine ]
        ]]
    suggestion:
        sys-apps/gamemode [[
            description = [ Utilize GameMode for possible performance improvements ]
        ]]
        x11-dri/mesa-demos [[
            description = [ Utilize glxinfo to detect graphics driver version ]
        ]]
"

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

