# Copyright 2018-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=FeralInteractive ] \
    meson \
    systemd-service

INIH_REV="745ada6724038cde32ff6390b32426cbdd5e532b"

SUMMARY="Daemon and library that allows games to request optimisations to be temporarily applied"
DESCRIPTION="
GameMode was designed primarily as a stop-gap solution to problems with the Intel and AMD CPU
powersave or ondemand governors, but is now able to launch custom user defined plugins, and is
intended to be expanded further, as there are a wealth of automation tasks one might want to apply.
"
DOWNLOADS+=" https://github.com/FeralInteractive/inih/archive/${INIH_REV}.tar.gz -> inih-${INIH_REV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-apps/systemd
    suggestion:
        group/games [[
            description = [ Allow renicing as an unpriviledged user being part of the games group ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Use-path_bindir-for-installing-gamemoderun.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Dwith-daemon=true
    -Dwith-dbus-service-dir=/usr/share/dbus-1/services
    -Dwith-examples=false
    -Dwith-pam-group=games
    -Dwith-systemd=true
    -Dwith-systemd-user-unit-dir=${SYSTEMDUSERUNITDIR}
)

src_prepare() {
    meson_src_prepare

    edo rmdir subprojects/inih
    edo ln -s ${WORKBASE}/inih-${INIH_REV} subprojects/inih
}

