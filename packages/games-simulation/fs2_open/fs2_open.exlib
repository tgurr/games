# Copyright 2017-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_cmake_modules_REPOSITORY="https://github.com/asarium/cmake-modules.git"
    SCM_librocket_REPOSITORY="https://github.com/asarium/libRocket.git"
    SCM_SECONDARY_REPOSITORIES="
        cmake_modules
        librocket
    "
    SCM_EXTERNAL_REFS="
        cmake/external/rpavlik-cmake-modules:cmake_modules
        lib/libRocket:librocket
    "
else
    CMAKE_SOURCE=${WORKBASE}/fs2open.github.com
fi

MY_PNV=${PN}_${PV//./_}

require github [ user=scp-fs2open project=fs2open.github.com release=release_${PV//./_} pnv=${MY_PNV}-source-Unix suffix=tar.gz ] \
    cmake [ api=2 ] \
    lua [ multibuild=false whitelist="5.1" ]

SUMMARY="FreeSpace2 Source Code Project"
HOMEPAGE+=" http://scp.indiegames.us/"

LICENCES="fs2_open"
SLOT="0"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
"

RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/jansson[>=2.2]
        media-libs/SDL:2[X]
        media-libs/libpng:=
        media-libs/openal
        providers:ffmpeg? ( media/ffmpeg )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libav? ( media/libav )
"

if ever is_scm ; then
    DEPENDENCIES+="
        build+run:
            media-libs/freetype:2
    "
else
    MYOPTIONS+="
        editor [[ description = [ Build the mission editor ] ]]
    "
    DEPENDENCIES+="
        build+run:
            editor? ( x11-libs/wxGTK:3.0[providers:gtk2] )
    "
fi

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DFFMPEG_USE_PRECOMPILED:BOOL=FALSE
    -DFSO_BUILD_APPIMAGE:BOOL=FALSE
    -DFSO_BUILD_INCLUDED_LIBS:BOOL=FALSE
    -DFSO_BUILD_TESTS:BOOL=FALSE
    -DFSO_BUILD_TOOLS:BOOL=FALSE
    -DFSO_USE_LUAJIT:BOOL=FALSE
)

if ever is_scm ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DENABLE_COTIRE:BOOL=TRUE
    )
fi

src_prepare() {
    cmake_src_prepare

    if ! ever is_scm ; then
        if option editor ; then
            edo sed \
                -e 's:#include <globalincs/pstypes.h>::g' \
                -i wxfred2/{frmFRED2,wxfred2}.cpp
        fi
    fi
}

src_configure() {
    if ever is_scm ; then
        local cmakeparams=(
            -DFSO_BUILD_QTFRED:BOOL=FALSE
        )
    else
        local cmakeparams=(
            -DFSO_BUILD_WXFRED2:BOOL=$(option editor TRUE FALSE)
        )
    fi

    ecmake \
        "${CMAKE_SRC_CONFIGURE_PARAMS[@]}" \
        "${cmakeparams[@]}"
}

src_install() {
    if ever is_scm ; then
        dobin bin/${PN}_3_8_1_x64
    else
        dobin bin/${MY_PNV}_x64

        if option editor ; then
            dobin bin/wxfred2_${PV//./_}_x64
        fi
    fi

    dodoc "${CMAKE_SOURCE}"/AUTHORS
}

pkg_postinst() {
    elog "${PN} requires a copy of the original FreeSpace2 resources. For more information see:"
    elog "http://wiki.hard-light.net/index.php/Manually_Installing_FreeSpace_2_Open"
}

